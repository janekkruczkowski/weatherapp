//
//  LicensesVC.swift
//  Weather
//
//  Created by Janek Kruczkowski on 19/10/2018.
//  Copyright © 2018 Janek Kruczkowski. All rights reserved.
//

import UIKit
import WebKit

class LicensesVC: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        let url = Bundle.main.url(forResource: "atribution", withExtension: "txt")!
        webView.loadFileURL(url, allowingReadAccessTo: url)
        let request = URLRequest(url: url)
        webView.load(request)
               

     
    }
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        dismiss(animated: true)
    }
    

}
