//
//  ViewController.swift
//  Weather
//
//  Created by Janek Kruczkowski on 17/10/2018.
//  Copyright © 2018 Janek Kruczkowski. All rights reserved.
//

import UIKit
import CoreLocation

class MainVC: UIViewController {

//MARK: IBOutlets

    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var tempLbl: UILabel!
    @IBOutlet weak var cloudsImg: UIImageView!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var conditionsLbl: UILabel!
    @IBOutlet weak var nextDaysTableView: UITableView!

//MARK: Properties

    fileprivate let weatherService = WeatherService.shared
    fileprivate let locationManager = CLLocationManager()

//MARK: View Lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()

        nextDaysTableView.delegate = self
        nextDaysTableView.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }


//MARK: Initial setup
    @objc func applicationDidBecomeActive(notification: NSNotification) {
        // Application is back in the foreground
        enableBasicLocationServices()

    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }


    fileprivate func initialSetup() {
        dateLbl.text = "Now, \(today())"
        tempLbl.text = "---°"
        conditionsLbl.text = ""
        cityLbl.text = "Feaching Location"

    }

    fileprivate func fetchWeatherForLocation(_ location: CLLocation) {

        weatherService.getCurrentWeather(lat: location.coordinate.latitude, lon: location.coordinate.longitude) { (success) in
            if success {

                self.tempLbl.fadeTransition(0.3)
                self.cityLbl.fadeTransition(0.3)
                self.conditionsLbl.fadeTransition(0.3)
                self.cloudsImg.fadeTransition(0.3)
                self.tempLbl.text = self.weatherService.currentWeather!.tempMax.temperatureString()
                self.cityLbl.text = self.weatherService.currentWeather!.place
                self.conditionsLbl.text = self.weatherService.currentWeather!.condition
                self.cloudsImg.image = UIImage(named: self.weatherService.currentWeather!.condition)


            }
        }


        weatherService.getForecast(lat: location.coordinate.latitude, lon: location.coordinate.longitude) { [weak self] (success) in
            if success {


                self?.nextDaysTableView.reloadData()


            }
        }

    }

//MARK: IBAction
    @IBAction func infoBtnPressed(_ sender: Any) {
        performSegue(withIdentifier: SEGUE, sender: self)

    }

//MARK: Helper
    fileprivate func today() -> String {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("EEEE, MMM d, yyyy")
        return dateFormatter.string(from: now)
    }

}

//MARK: TableView Delegate
extension MainVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherService.forecast.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CELL, for: indexPath) as? WeatherCell else {
            return UITableViewCell()
        }
        cell.setupView(weather: weatherService.forecast[indexPath.row])
        return cell

    }


}

//MARK: Location Manager Delegate
extension MainVC: CLLocationManagerDelegate {

    fileprivate func enableBasicLocationServices() {
        locationManager.delegate = self

        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // Request when-in-use authorization initially
            locationManager.requestWhenInUseAuthorization()
            break

        case .restricted, .denied:
            // Disable location features
            disableMyLocationBasedFeatures()
            break

        case .authorizedWhenInUse, .authorizedAlways:
            // Enable location features
            enableMyWhenInUseFeatures()
            break
        }
    }

    fileprivate func enableMyWhenInUseFeatures() {
        cityLbl.text = "Feaching Location"
        startReceivingLocationChanges()

    }

    fileprivate func disableMyLocationBasedFeatures() {


        locationManager.stopUpdatingLocation()
        cityLbl.text = "Location unavailable"

    }

    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted, .denied:
            disableMyLocationBasedFeatures()
            break

        case .authorizedWhenInUse:
            enableMyWhenInUseFeatures()
            break

        case .notDetermined, .authorizedAlways:
            break
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {


        if let location = locations.last {
            if location.horizontalAccuracy > 0 {
                self.locationManager.stopUpdatingLocation()
                DispatchQueue.main.async {

                    self.fetchWeatherForLocation(location)

                }
            }

        }

    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        disableMyLocationBasedFeatures()
    }

    func startReceivingLocationChanges() {
        let authorizationStatus = CLLocationManager.authorizationStatus()
        if authorizationStatus != .authorizedWhenInUse {
            enableBasicLocationServices()
            return
        }
        // Do not start services that aren't available.
        if !CLLocationManager.locationServicesEnabled() {
            // Location services is not available.
            return
        }
        // Configure and start the service.
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.distanceFilter = 100.0  // In meters.
        locationManager.startUpdatingLocation()

    }

}
    
    

