//
//  Constants.swift
//  Weather
//
//  Created by Janek Kruczkowski on 17/10/2018.
//  Copyright © 2018 Janek Kruczkowski. All rights reserved.
//

import Foundation

let CELL = "weatherCell"
let SEGUE = "licenseSegue"

//API

let API_URL = "https://api.openweathermap.org/data/2.5/"
let API_KEY = "appid=722ebf1de828d896aeaa162991760710&"
let WEATHER_UNITS = "units=metric&"

let ACTUAL_WEATHER_URL = "\(API_URL)weather?\(API_KEY)\(WEATHER_UNITS)"
let FORECAST_URL = "\(API_URL)forecast?\(API_KEY)\(WEATHER_UNITS)"

