//
//  Extensions.swift
//  Weather
//
//  Created by Janek Kruczkowski on 18/10/2018.
//  Copyright © 2018 Janek Kruczkowski. All rights reserved.
//

import UIKit


extension Double {

    func temperatureString() -> String {
        return "\(self)°C"
    }
}

extension UIView {
    func fadeTransition(_ duration: CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
        CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
}
