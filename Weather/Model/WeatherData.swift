//
//  WeatherData.swift
//  Weather
//
//  Created by Janek Kruczkowski on 17/10/2018.
//  Copyright © 2018 Janek Kruczkowski. All rights reserved.
//

import Foundation

struct WeatherData {

    var date: Date?
    var place: String
    var tempMin: Double
    var tempMax: Double
    var condition: String


    init(date: Double, tempMin: Double, tempMax: Double, condition: String, name: String) {
        self.date = Date.init(timeIntervalSince1970: date)
        self.tempMin = tempMin
        self.tempMax = tempMax
        self.condition = condition
        self.place = name
    }

    init(tempMin: Double, tempMax: Double, condition: String, name: String) {
        self.tempMin = tempMin
        self.tempMax = tempMax
        self.condition = condition
        self.place = name
    }


}
