//
//  WeatherService.swift
//  Weather
//
//  Created by Janek Kruczkowski on 17/10/2018.
//  Copyright © 2018 Janek Kruczkowski. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class WeatherService {

    static let shared = WeatherService()

    var currentWeather: WeatherData?
    var forecast = [WeatherData]()

    func getCurrentWeather(lat: Double, lon: Double, completion: @escaping (Bool) -> ()) {

        let url = "\(ACTUAL_WEATHER_URL)lat=\(lat)&lon=\(lon)"

        fetchApi(url, completion)


    }

    fileprivate func fetchApi(_ url: String, _ completion: @escaping (Bool) -> ()) {
        Alamofire.request(url).response { (response) in

            if let data = response.data {


                do {
                    let json = try JSON(data: data)


                    self.parseJSON(json: json)
                    completion(true)


                } catch {
                    print("error")
                    completion(false)
                }
            } else {

                completion(false)
            }


        }
    }

    func getForecast(lat: Double, lon: Double, completion: @escaping (Bool) -> ()) {

        let url = "\(FORECAST_URL)lat=\(lat)&lon=\(lon)"


        fetchApi(url, completion)


    }


    fileprivate func parseJSON(json: JSON) {


        if let dateArray = json["list"].array {
            let name = json["city"]["name"].stringValue
            for item in dateArray {

                let weatherDataItem = parseToWeatherData(name: name, json: item, date: item["dt"].doubleValue)
                forecast.append(weatherDataItem)

            }

        } else {
            let name = json["name"].stringValue
            currentWeather = parseToWeatherData(name: name, json: json)

        }


    }

    fileprivate func parseToWeatherData(name: String, json: JSON, date: Double? = nil) -> WeatherData {
        var weatherData: WeatherData
        let condition = json["weather"][0]["main"].stringValue
        let tempMin = json["main"]["temp_min"].doubleValue
        let tempMax = json["main"]["temp_max"].doubleValue
        if date != nil {
            weatherData = WeatherData(date: date!, tempMin: tempMin, tempMax: tempMax, condition: condition, name: name)
        } else {
            weatherData = WeatherData(tempMin: tempMin, tempMax: tempMax, condition: condition, name: name)
        }

        return weatherData
    }

}
