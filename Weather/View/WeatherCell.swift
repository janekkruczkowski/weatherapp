//
//  WeatherCell.swift
//  Weather
//
//  Created by Janek Kruczkowski on 17/10/2018.
//  Copyright © 2018 Janek Kruczkowski. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell {

    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var conditionLbl: UILabel!
    @IBOutlet weak var dayTmpLbl: UILabel!
    @IBOutlet weak var nightTmpLbl: UILabel!

    func setupView(weather: WeatherData) {

        dayLbl.text = prepareDateString(date: weather.date)
        conditionLbl.text = weather.condition
        dayTmpLbl.text = weather.tempMin.temperatureString()
        nightTmpLbl.text = weather.tempMax.temperatureString()
        itemImage.image = UIImage(named: "\(weather.condition) Mini")


    }

    fileprivate func prepareDateString(date: Date?) -> String {
        if date == nil {
            return ""

        }

        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("EEEE HH:mm")
        return dateFormatter.string(from: date!)

    }

}
